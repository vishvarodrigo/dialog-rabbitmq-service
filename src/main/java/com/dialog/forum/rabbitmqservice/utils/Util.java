package com.dialog.forum.rabbitmqservice.utils;

public class Util {

	// SUCCESS MESSAGES
	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String MESSAGE_SENT_SUCCESS_MESSAGE = "MESSAGE_SENT_SUCCESS";
	
	
	// FAILURE MESSAGES
	public static final String STATUS_FAILED = "FAILED";
	public static final String MESSAGE_SENT_FAILED_MESSAGE = "MESSAGE_SENT_FAILED";
	
	// CONFIGS
	public static final String MESSAGE_ROUTING_KEY = "message.sent";
	public static final String MESSAGE_QUEUE_NAME = "message.sent";
	public static final String MESSAGE_EXCHANGE_NAME = "message-exchange";
}

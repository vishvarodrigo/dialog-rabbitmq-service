package com.dialog.forum.rabbitmqservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DialogRabbitmqServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DialogRabbitmqServiceApplication.class, args);
	}
}

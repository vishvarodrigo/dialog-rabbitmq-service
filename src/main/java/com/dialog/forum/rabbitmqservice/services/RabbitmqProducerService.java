package com.dialog.forum.rabbitmqservice.services;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dialog.forum.rabbitmqservice.utils.Util;

@Service
public class RabbitmqProducerService {

	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	public RabbitmqProducerService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendMessage(String message){
		// SEND MESSAGE TO QUEUE
		rabbitTemplate.convertAndSend(Util.MESSAGE_ROUTING_KEY, message);
	}

}

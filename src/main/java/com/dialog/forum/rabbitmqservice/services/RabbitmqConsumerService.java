package com.dialog.forum.rabbitmqservice.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.dialog.forum.rabbitmqservice.utils.Util;

@Service
public class RabbitmqConsumerService {

	private Logger logger;
	
	public RabbitmqConsumerService() {
		logger = LoggerFactory.getLogger(this.getClass());
	}
	
	@RabbitListener(queues=Util.MESSAGE_QUEUE_NAME)
	public void updateStatus(String message) {
		logger.info("MESSAGE RECIEVED -> {}",message);
	}
}

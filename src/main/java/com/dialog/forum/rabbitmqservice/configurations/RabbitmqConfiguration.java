package com.dialog.forum.rabbitmqservice.configurations;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dialog.forum.rabbitmqservice.utils.Util;

@Configuration
public class RabbitmqConfiguration {

	@Bean
	public TopicExchange messageExchange() {
		return new TopicExchange(Util.MESSAGE_EXCHANGE_NAME);
	}

	@Bean
	public Queue messageQueue() {
		return new Queue(Util.MESSAGE_QUEUE_NAME);
	}

	@Bean
	public Binding declareBindingMessage() {
		return BindingBuilder.bind(messageQueue()).to(messageExchange()).with(Util.MESSAGE_ROUTING_KEY);
	}
}

package com.dialog.forum.rabbitmqservice.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dialog.forum.rabbitmqservice.services.RabbitmqProducerService;
import com.dialog.forum.rabbitmqservice.utils.Util;

@RestController
public class RabbitmqController {

	private RabbitmqProducerService rabbitmqService;
	
	@Autowired
	public RabbitmqController(RabbitmqProducerService rabbitmqService) {
		this.rabbitmqService = rabbitmqService;
	}
	
	@RequestMapping(value="/queue",method=RequestMethod.GET)
	public Map<String,Object> sendMessage(@RequestParam("message") String message){
		Map<String, Object> response = new HashMap<>();
		
		try {
			rabbitmqService.sendMessage(message);
			response.put("STATUS", Util.STATUS_SUCCESS);
			response.put("MESSAGE", Util.MESSAGE_SENT_SUCCESS_MESSAGE);
			
		}catch(Exception ex) {
			response.put("STATUS", Util.STATUS_FAILED);
			response.put("MESSAGE", Util.MESSAGE_SENT_FAILED_MESSAGE.concat(" ").concat(ex.getMessage()));
		}
		
		return response;
	}
}

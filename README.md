# Dialog Rabbitmq Service

This project initiated to understands the rabbitmq producer and consumer indiviudally

## Getting Started

Mostly this project valuable for spring boot backend developers who trying to use rabbitmq as message queue. To run this project you should have several dependencies

### Prerequisites

```
Spring Tool Suite(Recommended) or Eclipse with spring plugin
Gradle
RabbitMQ
```

### Installing

```
1) Install rabbitmq service to the device.
2) Expose port 5672 (default rabbitmq port)
3) Import project as a gradle project to the IDE
4) Refresh Gradle
5) Run the application as a spring boot application

```

### Access through URI

```
http://localhost:8080/queue?message={what ever the message}
```

### See result

```
See the spring boot application logs in the IDE
```

#### Developer Signature
Vishva Rodrigo @ Dialog Axiata PLC
